package test.tc_get;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

import base.base;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class get_newestreview extends base{

    @BeforeMethod
    public void getData() throws IOException, InterruptedException {
        initialization();

    }

    @Test
    public void getuserprofile_web()
    {
        // TODO Auto-generated method stub
        //BaseURL or Host
        RestAssured.baseURI=urlWeb;

        Response res=given().
                header("version", prop.getProperty("api_version")).
                header("key", prop.getProperty("key_web")).log().all().
                header("Authorization", prop.getProperty("token_web")).
                when().
                get("/newsreviews/1/10").
                then().assertThat().statusCode(200).and().contentType(ContentType.JSON).and().
                header("Server","nginx/1.10.3 (Ubuntu)").log().body().  //log untuk log request
                extract().response();

        String response_uname = res.path("data[0].username");
        System.out.println(response_uname);

        //result should not null
        Assert.assertNotSame(response_uname,null);
    }

}
