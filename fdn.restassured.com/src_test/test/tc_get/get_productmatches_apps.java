package test.tc_get;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

import base.base;

import java.io.IOException;

public class get_productmatches_apps extends base {

    @BeforeMethod
    public void getData() throws IOException, InterruptedException {
        initialization();

    }

    @Test
    public void getproductmatches_apps()
    {
        // TODO Auto-generated method stub
        //BaseURL or Host
        RestAssured.baseURI=prop.getProperty("base_url");

        Response res=given().
                header("version", prop.getProperty("api_version")).
                header("key", prop.getProperty("key_android")).log().all().
                header("Authorization", prop.getProperty("token_android")).
                when().
                get("/product_matches").
                then().assertThat().statusCode(200).and().contentType(ContentType.JSON).and().
                header("Server","nginx/1.10.3 (Ubuntu)").log().body().  //log untuk log request
                extract().response();

    }

}
