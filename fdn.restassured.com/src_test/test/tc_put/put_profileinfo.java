package test.tc_put;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import base.base;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class put_profileinfo extends base {

    @BeforeMethod
    public void setUp() throws IOException, InterruptedException {
        initialization();
    }

    @Test
    public void put_profile_info() {
        RestAssured.baseURI=url;

        given().
                contentType("multipart/form-data").
                header("version", prop.getProperty("api_version")).
                header("device", prop.getProperty("device_web")).
                header("Authorization", prop.getProperty("token_web")).
                multiPart("fullname", "test qaqa").
                multiPart("birthday", "17-08-1990").
                multiPart("brands", "65").

        when().
                put("/user/profile").then().
                assertThat().statusCode(200).and().contentType(ContentType.JSON).and(). //karna contenttypenya json
                header("Server","nginx/1.10.3 (Ubuntu)").log().body().  //log untuk log request
                extract().response();
//                //ambil dari jsoneditor online
//                        body("data.email",equalTo("fazlur.f.rahman@gmail.com")).and().
//                body("data.username", equalTo("putwid"));
    }

}
