package test.tc_post;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;

import base.base;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class post_userpost_apps extends base{

    @BeforeMethod
    public void getData() throws IOException, InterruptedException {
        initialization();

    }

    @Test
    public void run_add_product()
    {
        //Creating Issue/Defect

        RestAssured.baseURI= prop.getProperty("base_url");
        Response res= given().
                header("version", prop.getProperty("api_version")).
                header("device", prop.getProperty("device_android")).
                header("Authorization", prop.getProperty("token_android")).
                multiPart("image", "/Users/mac/Documents/File/Screenshot automation/fail/skrin.png").
                multiPart("caption", "fakkk").
//                multiPart("image", "").

                        when().
                        post("/user/tc_post").then().
                        assertThat().statusCode(200).and().contentType(ContentType.JSON).and().
                        header("Server","nginx/1.10.3 (Ubuntu)").log().body().  //log untuk log request
                extract().response();

    }

}
