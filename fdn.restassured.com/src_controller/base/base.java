package base;

import com.sun.org.apache.xerces.internal.impl.PropertyManager;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class base {

    public static Properties prop;

    public static String workingDir = System.getProperty("user.dir");

    public static String url = "http://api-dev.femaledaily.net/app/v1";
    public static String urlWeb = "http://api-dev.femaledaily.net/web/v1";
    public static String version = "1.5";
    public static String article_id = "131710";
    public static String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2ZXJzaW9uIjoiMS41IiwiZGV2aWNlIjoiMyIsImtleSI6ImNsaWVudDAzLVRTYnM5NHMzcTVIOVBQMnlOUEJyIiwicG9zdCI6eyJ1c2VyX2lkIjozNjQ2MjMsInVzZXJuYW1lIjoicWEwODAiLCJmdWxsbmFtZSI6InVzZXJuYW1ldGVzdCIsImVtYWlsIjoicWFzZWxlbml1bTgwQG1haWxpbmF0b3IuY29tIn0sImlhdCI6MTU0Mzc0MjA5MDE2OX0.H88CJlYrU95Hj_NpPhxEsygcjTbt4vdqb89D60pJ3bI";
    public static String user_id = "364623";
    public static String user_username = "qa080";

    //WEB
    public static int deviceWeb = 3;
    public static String keyWeb = "client03-TSbs94s3q5H9PP2yNPBr";

    //APPS
    public static String deviceAndroid = "1";
    public static String keyAndroid = "client01-pb3TVYisCGurD08ks3YW";
    public static String tokenApss = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2ZXJzaW9uIjoiMS41IiwiZGV2aWNlIjoiMyIsImtleSI6ImNsaWVudDAzLVRTYnM5NHMzcTVIOVBQMnlOUEJyIiwicG9zdCI6eyJ1c2VyX2lkIjozNjQ2MjMsInVzZXJuYW1lIjoicWEwODAiLCJmdWxsbmFtZSI6InRlc3QgcWFxYSIsImVtYWlsIjoicWFzZWxlbml1bTgwQG1haWxpbmF0b3IuY29tIn0sImlhdCI6MTU0Mzk4Mjk3MzQ4MX0.SOjkDTNGCMRW_CXXYAA9Gd07JxqDRTM-xmMqQnwpb1g";
//

//    private static PropertyManager instance;
//    private static final Object lock = new Object();
//    private static String propertyFilePath = System.getProperty("user.dir")+
//            "\\src\\test\\resources\\configuration.properties";
//    private static String wrongUsername;
//    private static String wrongPassword;

    public static void initialization() throws IOException, InterruptedException {
        prop=new Properties();

        FileInputStream ip= new FileInputStream(workingDir+"//env.properties");
        prop.load(ip);

        String device = prop.getProperty("deviceName");
        String platform = prop.getProperty("platformName");
        String serverLoc = prop.getProperty("server");
        String portNumber = prop.getProperty("port");
    }


}
